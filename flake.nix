{
  description = "nix-flake-templates";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils = {
    url = "github:numtide/flake-utils";
  };

  # The code below automatically sets up devShell and template outputs
  # for every subdirectory of `./templates`.
  #
  # Templates are named according to the name of the subdirectory.
  # Devshells use the names in their definitions in the template
  # directories.
  #
  # For example, the existence of ./templates/rust/flake.nix results in:
  #   - a top-level output templates.rust from this flake, which can be used by:
  #       nix flake init -t gitlab:kylesferrazza/nix-flake-templates#rust
  #   - a top-level output devShells.${system}.rust from this flake, which can be used by
  #       nix develop gitlab:kylesferrazza/nix-flake-templates#rust
  outputs =
    inputs@{
      self,
      nixpkgs,
      flake-utils,
    }:
    let
      templateBase = ./templates;
      getSubDirs =
        path:
        let
          contents = builtins.readDir path;
          filtered = nixpkgs.lib.attrsets.filterAttrs (k: v: v == "directory") contents;
          names = builtins.attrNames filtered;
          paths = builtins.map (name: path + "/${name}") names;
        in
        paths; # [ ./templates/a ./templates/b ]
      templatePaths = getSubDirs templateBase;
      importTemplateFlake = path: import (path + "/flake.nix");
      getTemplateName = path: builtins.baseNameOf path;
      getFlakeOutputs = flake: flake.outputs { inherit self nixpkgs flake-utils; };
      pathToShell =
        system: path:
        let
          name = getTemplateName path;
          flake = importTemplateFlake path;
          outputs = getFlakeOutputs flake;
          shell = outputs.devShell.${system};
        in
        {
          ${name} = shell;
        };
      pathToTemplate =
        path:
        let
          name = getTemplateName path;
          flake = importTemplateFlake path;
          description = flake.description;
          template = {
            ${name} = {
              inherit description path;
            };
          };
        in
        template;
      templateList = builtins.map pathToTemplate templatePaths;
      templates = nixpkgs.lib.attrsets.mergeAttrsList templateList;
      devShellsForSystemList = system: builtins.map (pathToShell system) templatePaths;
      devShellsForSystem = system: nixpkgs.lib.attrsets.mergeAttrsList (devShellsForSystemList system);
    in
    (flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        formatter = pkgs.nixfmt-rfc-style;
        devShells = devShellsForSystem system;
      }
    ))
    // {
      inherit templates;
    };
}
