{
  description = "A starter Django flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs =
    inputs@{
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShell = pkgs.mkShell {
          name = "django";
          buildInputs = with pkgs; [
            python3
            python3Packages.pip
            python3Packages.setuptools
            (pkgs.writeShellScriptBin "create-admin" ''
              export DJANGO_SUPERUSER_USERNAME="admin"
              export DJANGO_SUPERUSER_PASSWORD="admin"
              export DJANGO_SUPERUSER_EMAIL="admin@localhost"
              python3 manage.py createsuperuser --noinput
            '')
          ];
          shellHook = ''
            mkdir -p .venv
            export PIP_PREFIX=$(pwd)/.venv/pip_packages
            export PYTHONPATH="$PIP_PREFIX/${pkgs.python3.sitePackages}:$PYTHONPATH"
            export PATH="$PIP_PREFIX/bin:$PATH"
            unset SOURCE_DATE_EPOCH
          '';
          PYTHONSTARTUP = pkgs.writeText "startup.py" ''
            # from <appname>.models import *
            import pprint
            pp = pprint.PrettyPrinter(indent=2)
          '';
          PYTHONBREAKPOINT = "ipdb.set_trace";
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
