{
  description = "A starter Rails flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs =
    inputs@{
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        postgres_setup = ''
          export PGDATA=$PWD/postgres_data
          export PGHOST=$PWD/postgres
          export LOG_PATH=$PWD/postgres/LOG
          export PGDATABASE=postgres
          export DATABASE_CLEANER_ALLOW_REMOTE_DATABASE_URL=true
          if [ ! -d $PGHOST ]; then
            mkdir -p $PGHOST
          fi
          if [ ! -d $PGDATA ]; then
            echo 'Initializing postgresql database...'
            LC_ALL=C.utf8 initdb $PGDATA --auth=trust >/dev/null
          fi
        '';
        psql_setup_file = pkgs.writeText "setup.sql" ''
          DO
          $do$
          BEGIN
            IF NOT EXISTS ( SELECT FROM pg_catalog.pg_roles WHERE rolname = 'postgres') THEN
              CREATE ROLE postgres CREATEDB LOGIN;
            END IF;
          END
          $do$
        '';
        gem_setup = ''
          mkdir -p .nix-gems
          export GEM_HOME=$PWD/.nix-gems
          export GEM_PATH=$GEM_HOME
          export PATH=$GEM_HOME/bin:$PATH
          gem install --conservative bundler
        '';
        start_postgres = pkgs.writeShellScriptBin "start_postgres" ''
          pg_ctl start -l $LOG_PATH -o "-c listen_addresses= -c unix_socket_directories=$PGHOST"
          psql -f ${psql_setup_file} > /dev/null
        '';
        stop_postgres = pkgs.writeShellScriptBin "stop_postgres" ''
          pg_ctl -D $PGDATA stop
        '';
      in
      {
        devShell = pkgs.mkShell {
          name = "rails";
          buildInputs = with pkgs; [
            postgresql
            start_postgres
            stop_postgres
            ruby
          ];
          shellHook = ''
            ${postgres_setup}
            ${gem_setup}
          '';
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
