{
  description = "A starter NodeJS flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs =
    inputs@{
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShell = pkgs.mkShell {
          name = "nodejs";
          buildInputs = with pkgs; [
            nodejs
            nodePackages.pnpm
          ];
          shellHook = ''
            export PATH=$PWD/node_modules/.bin:$PATH
          '';
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
