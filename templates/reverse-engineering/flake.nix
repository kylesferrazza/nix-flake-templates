{
  description = "A starter reverse engineering flake";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs =
    inputs@{
      self,
      nixpkgs,
      flake-utils,
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShell = pkgs.mkShell {
          name = "reverse-engineering";
          buildInputs =
            (with pkgs; [
              ghidra
            ])
            ++ (with pkgs.python311Packages; [
              python
              angr
              angrcli
              angrop
              monkeyhex
              pip
              notebook
              jupyter
              pandas
            ]);
        };

        formatter = pkgs.nixfmt-rfc-style;
      }
    );
}
