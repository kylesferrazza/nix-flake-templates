# nix-flake-templates

```bash
nix flake init -t gitlab:kylesferrazza/nix-flake-templates
```

```bash
nix flake init -t gitlab:kylesferrazza/nix-flake-templates#rust
```

```bash
nix develop gitlab:kylesferrazza/nix-flake-templates#nodejs
$ pnpm create react-app
```
